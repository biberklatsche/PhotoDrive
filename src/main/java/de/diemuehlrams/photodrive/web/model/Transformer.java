package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author larswolfram
 */
public class Transformer {

    private Sorter sorter;
    private WebNameExtractor nameExtractor;
    private List<WebElement> transformedElements;
    
    public void setSorter(Sorter sorter){
        this.sorter = sorter;
    }
    
    public void setNameCreator(WebNameExtractor nameExtractor) {
        this.nameExtractor = nameExtractor;
    }
    
    public List<WebElement> run(File root) {
        if (root == null) {
            transformedElements = Collections.emptyList();
        }else{
            transformedElements = new LinkedList();
            sorter.run(root);
            transform(root);
        }
        return transformedElements;
    }

    private List<WebElement> transform(File root){
        int imageIndex = 0;
        for(File child : root.getChildren()){
            imageIndex = transform(child, null, imageIndex);
        }
        return transformedElements;
    }
    
    private int transform(File file, WebElement parent, int imageIndex) {
        if (file.isFolder()) {
            transformFolder(file, parent);
        }
        if (file.isImage()) {
            transformImage(file, parent, imageIndex++);
        }
        if (file.isText()) {
            transformText(file);
        }
        return imageIndex;
    }

    private void transformFolder(File file, WebElement parent) {
        int depth = getDepth(file);
        String name = nameExtractor.create(file);
        WebElement header = getLastTransformedElement();
        switch (depth) {
            case 1:
                header = WebElement.createHeader();
                header.setYear(name);
                transformedElements.add(header);
                break;
            case 2:
                if (header != null && header.getType().equals(WebElement.HEADER) && header.getHeaderName().isEmpty()) {
                    header.setHeaderName(name);
                } else {
                    header = WebElement.createSubHeader();
                    header.setYear(parent.getYear());
                    header.setHeaderName(name);
                    transformedElements.add(header);
                }
                break;
            case 3:
                if (header != null && (header.getType().equals(WebElement.HEADER) || header.getType().equals(WebElement.SUB_HEADER)) && header.getHeaderSubname().isEmpty()) {
                    header.setHeaderSubname(name);
                } else {
                    header = WebElement.createSubHeader();
                    header.setYear(parent.getYear());
                    header.setHeaderName(parent.getHeaderName());
                    header.setHeaderSubname(name);
                    transformedElements.add(header);
                }
                break;
        }
        int imageIndex = 0;
        for(File child :  file.getChildren()){
            imageIndex = transform(child, header, imageIndex);
        }
    }

    private WebElement getLastTransformedElement() {
        if (transformedElements.isEmpty()) {
            return null;
        } else {
            return transformedElements.get(transformedElements.size() - 1);
        }
    }

    private int getDepth(File file) {
        int depth = 0;
        File current = file;
        while(current.hasParent()){
            depth++;
            current = current.getParent();
        }
        return depth;
    }

    private void transformImage(File file, WebElement parent, int index) {
        WebElement image = WebElement.createImage();
        image.setThumbnail(file.getThumbnailLink());
        image.setLink(file.getLink());
        image.setYear(parent.getYear());
        image.setHeaderName(parent.getHeaderName());
        image.setIndex(index);
        image.setHeaderId(parent.getId());
        image.setHeaderSubname(parent.getHeaderSubname());
        image.setText(nameExtractor.create(file));
        transformedElements.add(image);
    }

    private void transformText(File file) {
        WebElement lastElem = getLastTransformedElement();
        if(lastElem != null){
            lastElem.setText(file.getText());
        }
    }
}
