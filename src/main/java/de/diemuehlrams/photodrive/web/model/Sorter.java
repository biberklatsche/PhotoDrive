/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.diemuehlrams.photodrive.web.model;

import de.diemuehlrams.photodrive.files.File;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author larswolfram
 */
public class Sorter{
    
    Comparator comp = new DefaultComparator();
    
    public void run(File root) {
        if(root != null){
            sort(root);
        }
    }
    
    private void sort(File file){
       
        Collections.sort(file.getChildren(), comp);
        if(file.isFolder()){
            for(File childFile : file.getChildren()){
                sort(childFile);
            }
        }
    }
    
    private class DefaultComparator implements Comparator<File> {

        @Override
        public int compare(File o1, File o2) {
            if(o1.isFolder() && !o2.isFolder()){
                return 1;
            }
            if(!o1.isFolder() && o2.isFolder()){
                return -1;
            }
            if(o1.isFolder() && o2.isFolder()){
                boolean o1StartsWithNumber = o1.getName().matches("^[0-9].*");
                boolean o2StartsWithNumber = o2.getName().matches("^[0-9].*");
                if(o1StartsWithNumber && !o2StartsWithNumber){
                    return 1;
                }
                else if(!o1StartsWithNumber && o2StartsWithNumber){
                    return -1;
                }
                else if(o1StartsWithNumber && o2StartsWithNumber){
                    return o2.getName().compareTo(o1.getName());
                }else{
                    String compare1 = getCompareString(o1.getName());
                    String compare2 = getCompareString(o2.getName());
                    return compare1.compareTo(compare2);
                }
            }else{
                if(o1.getName().matches("^[0-9!].*") || o2.getName().matches("^[0-9!].*")){
                    return o1.getName().compareTo(o2.getName());
                }else{
                    return o1.getCreationTimestamp().compareTo(o2.getCreationTimestamp());
                }   
            }
        }
        
        private String getCompareString(String s) {
            switch (s.toLowerCase()) {
                case "januar":
                    return "01";
                case "februar":
                    return "02";
                case "märz":
                    return "03";
                case "april":
                    return "04";
                case "mai":
                    return "05";
                case "juni":
                    return "06";
                case "juli":
                    return "07";
                case "august":
                    return "08";
                case "september":
                    return "09";
                case "oktober":
                    return "10";
                case "november":
                    return "11";
                case "dezember":
                    return "12";
                default:
                    return s;
            }
        }
    }
}
