package de.diemuehlrams.photodrive.web.model;

import java.util.UUID;

/**
 *
 * @author Lars
 */
public class WebElement {
    
    public static final String HEADER = "HEADER";
    public static final String SUB_HEADER = "SUB_HEADER";
    public static final String IMAGE = "IMAGE";
    
    private String year = "";
    private String headerName = ""; 
    private String headerSubname = "";
    private String text = "";
    private String thumbnail = "";
    private String link = "";
    private int index = 0;
    private String id = "";
    private String headerId = "";
    private String type = "";
    
    private WebElement(){}
    
    public static WebElement createHeader(){
        WebElement elem = new WebElement();
        elem.type = HEADER;
        elem.id = UUID.randomUUID().toString();
        return elem;
    }
    
    public static WebElement createSubHeader(){
        WebElement elem = new WebElement();
        elem.type = SUB_HEADER;
        elem.id = UUID.randomUUID().toString();
        return elem;
    }
    
    public static WebElement createImage() {
        WebElement elem = new WebElement();
        elem.type = IMAGE;
        elem.id = UUID.randomUUID().toString();
        return elem;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the headerName
     */
    public String getHeaderName() {
        return headerName;
    }

    /**
     * @param headerName the headerName to set
     */
    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    /**
     * @return the headerSubname
     */
    public String getHeaderSubname() {
        return headerSubname;
    }

    /**
     * @param headerSubname the headerSubname to set
     */
    public void setHeaderSubname(String headerSubname) {
        this.headerSubname = headerSubname;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * @param thumbnail the thumbnail to set
     */
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the path to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the headerId
     */
    public String getHeaderId() {
        return headerId;
    }

    /**
     * @param headerId the headerId to set
     */
    public void setHeaderId(String headerId) {
        this.headerId = headerId;
    }
}
