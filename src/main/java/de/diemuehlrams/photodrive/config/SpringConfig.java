package de.diemuehlrams.photodrive.config;

import de.diemuehlrams.photodrive.web.model.WebNameExtractor;
import de.diemuehlrams.photodrive.web.model.Sorter;
import de.diemuehlrams.photodrive.web.model.Model;
import de.diemuehlrams.photodrive.web.model.Transformer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("de.diemuehlrams.photodrive")
public class SpringConfig {
   @Bean 
   public Model model(){
      Model model = Model.getInstance();
      return model;
   }
   
   @Bean 
   public Transformer transformer(){
      Transformer transformer = new Transformer();
      transformer.setSorter(new Sorter());
      transformer.setNameCreator(new WebNameExtractor());
      return transformer;
   }

}
