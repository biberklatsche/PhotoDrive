package de.diemuehlrams.photodrive.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import de.diemuehlrams.photodrive.exception.PDException;
import de.diemuehlrams.photodrive.files.File;
import de.diemuehlrams.photodrive.files.FileFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lars
 */
@Primary
@Service
public class GoogleDriveFileService implements FileService {

    private static final Logger LOG = Logger
            .getLogger(FileService.class);
    private static final String APPLICATION_NAME = "DieMuehlrams-photodrive/1.0";
    private static final String P12_FILE_NAME = "diemuehlrams-893554fa24c4.p12";
    private static final String SERVICE_ACCOUNT_ID = "48690473184-r54bnel42c6v7nt170o39ma3d5sfmit3@developer.gserviceaccount.com";
    private static final String HOME_FOLDER_ID = "0BwXXY_jTpVqnWTRSN1RUMnFEWUk";

    private static final JsonFactory JSON_FACTORY = JacksonFactory
            .getDefaultInstance();
    private HttpTransport httpTransport;
    private Drive drive;

    private Drive createDriveService() throws PDException {
        try {
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            GoogleCredential credential;
            credential = new GoogleCredential.Builder()
                    .setTransport(httpTransport)
                    .setJsonFactory(JSON_FACTORY)
                    .setServiceAccountId(SERVICE_ACCOUNT_ID)
                    .setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE))
                    .setServiceAccountPrivateKeyFromP12File(new java.io.File(FileService.class
                                    .getClassLoader()
                                    .getResource(P12_FILE_NAME).getPath()))
                    .build();
            return new Drive.Builder(httpTransport, JSON_FACTORY, null)
                    .setApplicationName(APPLICATION_NAME)
                    .setHttpRequestInitializer(credential).build();
        } catch (IOException ex) {
            throw PDException.createAndLog(LOG,
                    "Error while init GoogleDriveService. Missing File %s.",
                    P12_FILE_NAME, ex);
        } catch (GeneralSecurityException ex) {
            throw PDException.createAndLog(LOG,
                    "Error while init GoogleDriveService.", ex);
        }
    }

    private void setProxy() {
        System.setProperty("http.proxyHost", "");
        System.setProperty("http.proxyPort", "8080");
        System.setProperty("https.proxyHost", "");
        System.setProperty("https.proxyPort", "8080");
        System.setProperty("http.proxyUser", "");
        System.setProperty("http.proxyPassword", "");

    }

    @Override
    public File getRoot() throws PDException {
        if(drive == null){
            drive = createDriveService();
        }
        File root = FileFactory.createRoot();
        fillFolder(HOME_FOLDER_ID, root);
        return root;
    }

    private void fillFolder(String folderId, File folder) throws PDException {
        try {
            Drive.Files.List request = drive
                    .files()
                    .list()
                    .setQ(String.format("'%s' in parents", folderId));
            List<File> results = new LinkedList();
            do {
                try {
                    FileList files = request.execute();
                    for (com.google.api.services.drive.model.File f : files.getItems()) {
                        File file = null;
                        if (f.getMimeType().equals("application/vnd.google-apps.folder")) {
                            file = FileFactory.createFolder(f.getTitle(), folder);
                            fillFolder(f.getId(),file);
                        } else if (f.getMimeType().contains("image")) {
                            file = FileFactory.createImageFile(f.getTitle(), folder);
                            file.setThumbnailLink(f.getThumbnailLink());
                            String imageLink = createLink(f.getThumbnailLink());
                            file.setLink(imageLink);
                        } else if (f.getMimeType().contains("text/plain")) {
                            file = FileFactory.createTextFile(f.getTitle(), folder);
                            file.setText(downloadContent(f.getDownloadUrl()));
                        }
                        DateTime creationDate = f.getCreatedDate();
                        if(file != null && creationDate != null){
                            file.setCreationTimestamp(creationDate.getValue());
                        }
                    }
                    request.setPageToken(files.getNextPageToken());
                } catch (IOException e) {
                    LOG.error("An error occurred while get folders: ", e);
                    request.setPageToken(null);
                }
            } while (request.getPageToken() != null
                    && request.getPageToken().length() > 0);
        } catch (IOException ex) {
            throw PDException.createAndLog(LOG, "Error while get files from drive.", ex);
        }
    }

    private String downloadContent(String downloadUrl) {
        StringBuilder b = new StringBuilder();
        try {
            HttpResponse resp = drive.getRequestFactory().buildGetRequest(new GenericUrl(downloadUrl)).execute();
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(resp.getContent(), "UTF-8"))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    b.append(inputLine);
                }
            } catch (IOException ex) {
                LOG.error("Error while reading content of file " + downloadUrl, ex);
            }
        } catch (MalformedURLException ex) {
            LOG.error("Malformed URL: " + downloadUrl, ex);
        } catch (IOException ex) {
            LOG.error("Error while reading content of file " + downloadUrl, ex);
        }
        return b.toString();
    }
    
    private String createLink(String thumbnailLink){
        return thumbnailLink.replaceFirst("=s220", "=s1024");
    }
}
