angular.module('dmCarrousel', [])
    .directive('dmCarrousel', ['$compile', '$document', '$timeout', '$location', '$anchorScroll', function ($compile, $document, $timeout, $location, $anchorScroll) {

        return {
            restrict: 'E',
            link: function (scope, iElement, iAttributes, containerCtrl) {
                scope.$watch("currentImageId", function () {
                    var currentImageIndex = 0;
                    var totalSlides = 0;
                    var animating = false;
                    var tpl = '<div class="dm-car-container">';
                    tpl += '<ul class="dm-car">';

                    angular.forEach(scope.elements, function (image, index) {
                        var headerId = image.headerId;
                        if (image.link &&
                            scope.currentHeaderId === headerId) {
                            tpl += '<li class="dm-car-item"><div class="dm-car-image" data-src="' + image.link + '"></div></li>';
                            totalSlides++;
                        }
                    });
                    tpl += "</ul>";

                    tpl += '<div class="dm-car-controls">' +
                        '  <span class="dm-car-control dm-car-control-prev" ng-click="goToPrev()">&lsaquo;</span>' +
                        '  <span class="dm-car-control dm-car-control-next" ng-click="goToNext()">&rsaquo;</span>' +
                        '</div>';

                    tpl += '</div>'
                    iElement.empty();
                    iElement.append($compile(angular.element(tpl))(scope));

                    var slides = iElement[0].querySelectorAll("li");

                    goToSlide(scope.currentImageIndex);
                    animating = false;

                    scope.goToNext = function () {

                        var index = currentImageIndex;

                        //console.log("Go to next slide");
                        if (currentImageIndex < totalSlides - 1) {
                            index++;

                        }
                        else {
                            index = 0;
                        }

                        goToSlide(index);

                    }

                    scope.goToPrev = function () {

                        var index = currentImageIndex;

                        //console.log("Go to previous slide");
                        if (currentImageIndex > 0) {
                            index--;
                        }
                        else {
                            index = slides.length - 1;
                            //console.log(currentImageIndex)
                        }

                        goToSlide(index);
                    }

                    function goToSlide(index) {

                        //console.log("Go to slide " + index);

                        //check if the carrousel already is animating
                        if (animating) {
                            //console.error("already animating");
                            return;
                        }
                        else {
                            animating = true;
                            currentImageIndex = index;
                        }


                        //TODO - find a better way to remove class names
                        angular.forEach(slides, function (slide, index) {
                            angular.element(slide).removeClass("current prev next");
                        })

                        if (index < totalSlides && index >= 0) {

                            angular.element(slides[index]).addClass("current");
                            var image = angular.element(slides[index]).find(".dm-car-image");
                            loadImage(image);
                            angular.element(slides[index]).bind('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
                                transitionDone();
                            });

                        }
                        else {
                            //console.error("Image index out of range");
                        }

                        if (index < totalSlides - 1) {
                            angular.element(slides[index + 1]).addClass("next");
                            var image = angular.element(slides[index + 1]).find(".dm-car-image");
                            loadImage(image);
                        }
                        else {
                            angular.element(slides[0]).addClass("next");
                            var image = angular.element(slides[index + 1]).find(".dm-car-image");
                            loadImage(image);
                        }

                        if (index > 0) {
                            angular.element(slides[index - 1]).addClass("prev");
                            var image = angular.element(slides[index - 1]).find(".dm-car-image");
                            loadImage(image);
                        }
                        else {
                            angular.element(slides[slides.length - 1]).addClass("prev");
                            var image = angular.element(slides[index - 1]).find(".dm-car-image");
                            loadImage(image);
                        }

                    }

                    function loadImage(element) {

                        var dataSrc = element.data("src");
                        var style = element.attr("style");
                        if ((style === undefined || style === '') && dataSrc != undefined) {

                            element.attr("style", "background-image:url(" + dataSrc + ");");

                        }

                    }

                    function transitionDone() {

                        animating = false;

                        var current = slides[currentImageIndex];

                        current.removeEventListener('webkitTransitionEnd', function () {
                            transitionDone()
                        });
                        current.removeEventListener('oTransitionEnd', function () {
                            transitionDone()
                        });
                        current.removeEventListener('transitionEnd', function () {
                            transitionDone()
                        });

                    }

                    scope.carrouselClose = function () {
                        $(".navbar-fixed-top").addClass("top-nav-collapse");
                        $("body").addClass("fade-out");
                        $("#carrousel").addClass("scale-small");
                        $timeout(function () {
                            $("body").removeClass("fade-out");
                            $("#carrousel").removeClass("scale-small");
                            $("body").addClass("fade-in");
                            $("#" + scope.currentImageId).addClass("scale-big-reverse");
                            scope.isCarrouselOpen = false;
                            scope.busy = false;
                            $location.hash(scope.currentImageId);
                            $anchorScroll();
                        }, 250);
                        $timeout(function () {
                            $("body").removeClass("fade-in");
                            $("#" + scope.currentImageId).removeClass("scale-big-reverse");
                        }, 500);
                    };

                });

                $document.bind("keydown", function (event) {
                    if (scope.isCarrouselOpen) {
                        if (event.keyCode === 37) {
                            scope.goToPrev();
                        }
                        if (event.keyCode === 39) {
                            scope.goToNext();
                        }
                        if (event.keyCode === 27) {
                            scope.carrouselClose();
                        }
                    }
                });
            }
        };
    }]);