var app = angular
    .module('PhotoDriveApp', ['ngResource', 'infinite-scroll', 'dmCarrousel']);
app.config(function ($locationProvider) {
    $locationProvider.html5Mode({enabled: true, requireBase: false});
})
    .constant('PAGING_SIZE', 20)
    .service('ElementService', function ($resource) {
        return {
            getElements: function (from, offset) {
                var folderResource = $resource('private/elements', {from: from, offset: offset}, {
                    query: {method: 'GET', params: {}}
                });
                return folderResource.query(
                    function () {
                        //handle success
                    },
                    function () {
                        window.location.href = './login.html';
                    });
            },
            reload: function () {
                var folderResource = $resource('private/reload', {}, {
                    query: {method: 'GET', params: {}}
                });
                return folderResource.query(
                    function () {
                        //handle success
                    },
                    function () {
                        window.location.href = './login.html';
                    });
            }
        };
    })
    .controller('ElementController', function ($scope, $timeout, ElementService, PAGING_SIZE) {
        $scope.from = 0;
        $scope.elements = [];
        $scope.busy = false;
        $scope.isAllLoaded = false;
        $scope.isCarrouselOpen = false;
        $scope.currentHeaderId = '';
        $scope.currentImageId = '';
        $scope.currentImageIndex = 0;

        $scope.reload = function () {
            $scope.busy = true;
            ElementService.reload().$promise.then(function () {
                $scope.busy = false;
            });
        };

        $scope.paging = function () {
            if (!$scope.isAllLoaded) {
                $scope.busy = true;
                ElementService.getElements($scope.from, PAGING_SIZE).$promise.then(function (elementList) {
                    var f = elementList.elements;
                    $scope.elements = $scope.elements.concat(f);
                    $scope.from += PAGING_SIZE;
                    $scope.maxCount = elementList.maxCount;
                    $scope.busy = false;
                    $scope.isAllLoaded = $scope.maxCount === $scope.elements.length;
                });
            }
        };

        $scope.imageClicked = function (element) {
            $scope.currentHeaderId = element.headerId;
            $scope.currentImageId = element.id;
            $scope.currentImageIndex = element.index;
            $scope.busy = true;
            $("body").addClass("fade-out");
            $("#" + $scope.currentImageId).addClass("scale-big");
            $timeout(function () {
                $("body").removeClass("fade-out");
                $("#" + $scope.currentImageId).removeClass("scale-big");
                $("body").addClass("fade-in");
                $("#carrousel").addClass("scale-small-reverse");
                $scope.isCarrouselOpen = true;
            }, 250);
            $timeout(function () {
                $("body").removeClass("fade-in");
                $("#carrousel").removeClass("scale-small-reverse");
            }, 500);
        };
    });
