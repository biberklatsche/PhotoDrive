var app = angular
        .module('PhotoDriveApp', ['ngResource', 'infinite-scroll', 'dmCarrousel']);

app.config(function ($locationProvider) {
    $locationProvider.html5Mode({enabled: true, requireBase: false});
})
        .controller('ElementController', function ($scope, $timeout) {
            $scope.testCounter = 0;
            $scope.busy = false;
            $scope.isCarrouselOpen = false;
            $scope.isAllLoaded = false;
            $scope.currentHeaderId = '';
            $scope.currentImageId = '';
            $scope.currentImageIndex = 0;
            $scope.from = 0;
            $scope.elements = [
                {"id": 0, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "", "type": "HEADER"},
                {"headerId": 0, "id": 10, "index": 0, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "001.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                {"headerId": 0, "id": 11, "index": 1, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "002.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                {"headerId": 0, "id": 12, "index": 2, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "003.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                {"headerId": 0, "id": 13, "index": 3, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "004.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                {"headerId": 0, "id": 14, "index": 4, "year": "2015", "headerName": "Januar", "headerSubname": "Merzien", "text": "004.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"}

            ];

            $scope.reload = function () {

            };

            $scope.paging = function () {
                $scope.busy = true;
                if ($scope.testCounter < 10) {
                    $scope.testCounter++;
                    $scope.elements = $scope.elements.concat([
                        {"year": "2014", "headerName": "Januar", "headerSubname": "Wernigerodemitdreim", "text": "", "type": "HEADER"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "Februar", "headerSubname": "Merzien", "text": "Das ist eine Beschreibung für den Januar.", "type": "SUB_HEADER"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"},
                        {"year": "2014", "headerName": "P1090990.jpg", "link": "../../../resources/images/P1090990.jpg", "thumbnail": "../../../resources/images/P1090990.jpg", "type": "IMAGE"}
                    ]);
                } else {
                    $scope.isAllLoaded = true;
                }
                $scope.busy = false;
            };

        $scope.imageClicked = function (element) {
            $scope.currentHeaderId = element.headerId;
            $scope.currentImageId = element.id;
            $scope.currentImageIndex = element.index;
            $scope.busy = true;
            $("body").addClass("fade-out");
            $("#" + $scope.currentImageId).addClass("scale-big");
            $timeout(function () {
                $("body").removeClass("fade-out");
                $("#" + $scope.currentImageId).removeClass("scale-big");
                $("body").addClass("fade-in");
                $("#carrousel").addClass("scale-small-reverse");
                $scope.isCarrouselOpen = true;
            }, 250);
            $timeout(function () {
                $("body").removeClass("fade-in");
                $("#carrousel").removeClass("scale-small-reverse");
            }, 500);
        };
        });
